package facebookTesting;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class FramesConcept {

	public static void main(String[] args) {
	
		System.setProperty("webdriver.chrome.driver","./src/facebookTesting/driver/chromedriver.exe");
		WebDriver driver=new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("http://demo.automationtesting.in/Frames.html");
		
		//By Index
		
		driver.switchTo().frame(0);
		WebElement textbox = driver.findElement(By.xpath("//input[@type='text'][1]"));
		textbox.sendKeys("Manoj");
		
		//By Name
		
		driver.switchTo().frame("singleframe");
		WebElement textbox1 = driver.findElement(By.xpath("//input[@type='text'][1])"));
		textbox1.sendKeys("Manoj");
		
		//By WebElement
		

	}

}
