package facebookTesting;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public class RepetitiveWords {

	public static void main(String[] args) {
		
		String s = "Welcome to to java java java";
		String[] allWords = s.split(" ");
		
		Map<String,Integer> m = new LinkedHashMap<>();
		for (String word : allWords) {
			
		if(m.containsKey(word))
		{
			Integer in = m.get(word);
			m.put(word, in+1);
		}
		else
		{
			m.put(word, 1);
		}
		}
		System.out.println(m);
	
		Set<Entry<String,Integer>> entrySet = m.entrySet();
		for (Entry<String, Integer> entry : entrySet) {
			if(entry.getValue()>1)
			System.out.println(entry);
			
		}
	}
	
		
		

	}

