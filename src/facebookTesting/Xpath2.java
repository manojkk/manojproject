package facebookTesting;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class Xpath2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		System.setProperty("webdriver.chrome.driver","./src/facebookTesting/driver/chromedriver.exe");
		WebDriver driver=new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://www.facebook.com/");
		
		driver.findElement(By.xpath("//input[contains(@id,'email')]")).sendKeys("xyz");
		driver.findElement(By.xpath("//input[contains(@id,'pass')]")).sendKeys("12345");
		
		driver.findElement(By.xpath("//input[contains(@name,'firstname')]")).sendKeys("Vinoth");
		driver.findElement(By.xpath("//input[contains(@name,'lastname')]")).sendKeys("Kumar");
		
		WebElement day =  driver.findElement(By.id("day"));
		Select s= new Select(day);
		s.selectByValue("11");
		
		WebElement month = driver.findElement(By.id("month"));
		Select s2 = new Select(month);
		s2.selectByVisibleText("Aug");
		
		WebElement year = driver.findElement(By.id("year"));
		Select s3 = new Select(year);
		s3.selectByIndex(29);
		
	}

}
