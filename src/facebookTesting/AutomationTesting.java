package facebookTesting;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class AutomationTesting {

	public static void main(String[] args) throws Exception {
		
		System.setProperty("webdriver.chrome.driver","./src/facebookTesting/driver/chromedriver.exe");
		WebDriver driver=new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("http://automationpractice.com/index.php");
	
		driver.findElement(By.linkText("Sign in")).click();
		Thread.sleep(3000);
		driver.findElement(By.id("email_create")).sendKeys("manojk11@gmail.com");
		driver.findElement(By.id("SubmitCreate")).click();
		Thread.sleep(5000);
		
		driver.findElement(By.id("id_gender1")).click();
		driver.findElement(By.name("customer_firstname")).sendKeys("Manoj");
		driver.findElement(By.name("customer_lastname")).sendKeys("Kumar");
		driver.findElement(By.id("passwd")).sendKeys("abc1234");
		
		WebElement day = driver.findElement(By.id("days"));
		Select s=new Select(day);
		s.selectByValue("11");
		
		WebElement month = driver.findElement(By.id("months"));
		Select s2=new Select(month);
		s2.selectByVisibleText("August ");
		
		WebElement year = driver.findElement(By.id("years"));
		Select s3=new Select(year);
		s3.selectByValue("1991");
		
		driver.findElement(By.id("uniform-newsletter")).click();
		driver.findElement(By.id("optin")).click();
	
		driver.findElement(By.id("company")).sendKeys("IBM");
		
		driver.findElement(By.name("address1")).sendKeys("No.12, Block 1A - 3rd floor");
		driver.findElement(By.id("address2")).sendKeys("DLF, Ramapuram, Chennai");
		driver.findElement(By.name("city")).sendKeys("Chennai");
		
		WebElement state = driver.findElement(By.id("id_state"));
		Select s4=new Select(state);
		s4.selectByValue("9");
		
		driver.findElement(By.name("postcode")).sendKeys("00000");
		driver.findElement(By.name("phone")).sendKeys("044-24916942");
		driver.findElement(By.name("phone_mobile")).sendKeys("9840632429");
		driver.findElement(By.id("alias")).sendKeys("Not Applicable");
		driver.findElement(By.id("submitAccount")).click();
	
	}

}
